### 1. Assumptions

1.1. It has been discussed via email, but possibly it worth mentioning.
"Sport name" must be a filter, but not a namespace.
E.g. the following request from the docs:
```
http://example.com/api/match/football?ordering=startTime
```
Must be replaced with:
```
http://example.com/api/match/?football&ordering=startTime
```

1.2. Both "NewEvent" and "UpdateOdds" events passed via POST request to objects collection.
According to the docs, the both requests applied to the matches collection namespace (/match/).

I have to mention that "message_type" parameter is redundant in the messages.
As event creation could be performed via POST request at /match/ and odds could be updated via PATCH at /match/<match_id>. But I assumed,
assumed that the messages structure is selected by the external provides, so we just need to work with it.

1.3. Ordering filters sort the data in descending order.
According to the example at page 2, results ordered by "startTime", 
returned in descending order:
```
http://example.com/api/match/football?ordering=startTime

output format:
[{
"id": 994839351740,
"url": "http://example.com/api/match/994839351740",
"name": "Real Madrid vs Barcelona",
"startTime": "2018-06-20 10:30:00"
},
{
"id": 994839351788,
"url": "http://example.com/api/match/994839351788",
"name": "Cavaliers vs Lakers",
"startTime": "2018-01-15 22:00:00"
}]
```

I took a consideration that the rule of showing results in descending order applies to all of the ordering filters.

1.4. As it's mentioned in the docs, that there will be always one market "Winner", I assummed, that
it's not allowed to create new markets via POST request (match creation), so the default marked,
(along with a few demo matches), is loaded to the DB authmatically on the deployment from match_api/fixtures/default_data.json


### 2. Implementation
The project implemented on python3.6 using django-rest-framework in order to use be generic as possible,
as it allows to provide a better robustness and testability, comparing to implementing everything from the scratch.

I've not been working with Django for 3 years 
(was using more low-level libs like "requests" for web), and hoped that django-rest-framework 
handles objects relations more comprehensive from the box, so it would allow to avoid some extra 
code related to Selections data handling.

### 3. Testing

Unit tests can be run as: 
```
$ cd betbright
$ python manage.py test match-api.tests

...
----------------------------------------------------------------------
Ran 36 tests in 0.248s

OK

```

Views unit tests demonstrate the testing practices, however
multiple views tests are just added as mocks (self-documenting test names) 
for time-saving purposes. Models and Serializer tests are mocks-only.


### 4. Deployment

A simple fabric script is used to automate the basic deployment process.

```
$ pip install -r requirements.py # Or just install fabric via "pip3 install fabric3"
$ cd betbright/deploy
$ fab deploy

[127.0.0.1] Executing task 'deploy'
...
...

Done.
Disconnecting from 127.0.0.1... done.
```

Default host is '127.0.0.1' (please, note that if you keep the default host, you'll need to have SSH server installed).
A simple set of fixtures will be loaded automatically to the database for demonstration purposes.
Please, note that deployment script has been tested on Ubuntu 18.04 only.

### 5. Running
Debug server can be run via:
```
$ source venv/bin/activate
$(venv) cd betbright
$(venv) python manage.py runserver
```

For real-life production a the web server and gateway must be configured in hosting OS.

Django-rest-framework provides a fancy representation of the views data,
which can be converted to pure JSON by switching settings.DEBUG to False (usually production environment 
can be detected automatically, but I assume that this project will run in dev mode only).


### 6. Improvements

6.1. Unit tests must be improved.

6.2. Doc strings can be extended.

6.3. Django still populates some redundant tables, which can be skipped by disabling some middleware in settings.py

6.4. The deployment script can be widely improved for a real-life production environment. 
Beside that it can run without human participation, if public keys will be uploaded 
to host and bitbucket server.

6.5. Input data validation and error represenation logic can be revised.
