"""Betbright coding challenge project URLs module."""
from django.conf.urls import url, include


urlpatterns = [
    url('', include('match-api.urls', namespace='match-api'))
]
