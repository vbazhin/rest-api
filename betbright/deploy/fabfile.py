"""Primitive script for demonstration automation demonstration.
Can be improved in accordance with the needs."""

import os
from fabric.contrib.files import exists
from fabric.api import cd, env, local, run, sudo

# Repo URL. Currently repo is private, I'll add a few users, or make it public.
REPO_URL = 'https://bitbucket.org/vbazhin/rest-api/src/master/betbright/match-api/'

# Directory to deploy the project on host.
DEPLOYMENT_DIR = os.path.join(os.path.expanduser('~'), 'match-api')
PROJECT_NAME = 'betbright'

# Default host. Use your host.
env.hosts = [ '127.0.0.1' ]

def deploy():
    run('mkdir -p {}'.format(DEPLOYMENT_DIR))
    with cd(DEPLOYMENT_DIR):
        clone_repo()
        install_virtualenv()
        update_virtualenv()
        migrate_database()
        # Loading DB fixtures for demo purposes (valid for coding challenge only).
        load_db_fixtures()

def clone_repo():
    if exists('.git'):
        run('git fetch')
    else:
        run('git clone {} .'.format(REPO_URL))
    current_commit = local("git log -n 1 --format=%H", capture=True)
    run(f'git reset --hard {current_commit}')

def install_virtualenv():
    sudo('apt-get install python3-venv')

def update_virtualenv():
    if not exists('venv/bin/pip'):
        run('python3.6 -m venv venv')
    run('./venv/bin/pip install -r requirements.txt')

def migrate_database():
    run('./venv/bin/python {}/manage.py migrate --noinput'.format(PROJECT_NAME))


def load_db_fixtures():
    run('./venv/bin/python {project}/manage.py loaddata {project}'
        '/match-api/fixtures/default_data.json'.format(project=PROJECT_NAME))
