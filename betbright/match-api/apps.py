from django.apps import AppConfig


class WebApiConfig(AppConfig):
    name = 'match-api'
