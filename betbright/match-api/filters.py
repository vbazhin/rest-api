"""Custom filters for REST API view."""
from rest_framework.filters import OrderingFilter


class MatchOrderingFilter(OrderingFilter):
    """The class reversed the ordering.

    As it's shown in the example in the docs (page 2),
    objects are being ordered in descending order,
    so the default behavior of OrderingFilter must be tweaked.
    Also, the class converts all ordering filters to lowercase.

    Comment: rest_framework.filters.SearchFilter accepted the query params
    filter name params case-insensitive by default,
    but rest_framework.filters.OrderingFilter workes in case-sensitive order
    (e.g. "?ordering=starttime" worked, but "?ordering=startTime" didn't).

    I considered that an inconsistence, so I forked django-rest-framework repo and pushed very basic tweak:
    https://github.com/vbazhin/django-rest-framework/commit/3906e40343f9e4eebe2980b62e30218721f29e80
    Maybe, I'll create a pull-request to the main django-rest-framework repo as soon,
    as I'll think over the implied semantics of SearchFilter and improve the solution.
    """

    def get_ordering(self, request, queryset, view):
        """Re-defined parent's class method,
         To return results in reversed ordering and convert filters to lowercase.
        """
        params = request.query_params.get(self.ordering_param)
        if params:
            fields = [param.lower().strip() for param in params.split(',')]
            ordering = self.remove_invalid_fields(queryset, fields, view, request)
            if ordering:
                return self._reverse_ordering(ordering)

    @staticmethod
    def _reverse_ordering(ordering):
        """Apply descending ordering, as it's shown in the coding challenge docs."""
        reversed_ordering = []
        for order in ordering:
            if order.startswith('-'):
                order = order.lstrip('-')
            else:
                order = '-' + order
            reversed_ordering.append(order)
        return reversed_ordering
