"""Web REST API model classes declaration."""
from django.db import models, transaction
from django.shortcuts import get_object_or_404


class Sport(models.Model):
    """Store Sport class entry."""

    id = models.BigIntegerField(primary_key=True, unique=True)
    name = models.CharField(max_length=200, unique=True, blank=False)


class Market(models.Model):
    """Store Market entry."""

    id = models.BigIntegerField(primary_key=True)
    name = models.CharField(max_length=200, blank=False)

    def create_selections(self, match, selections_data):
        """Create selection objects related to current market and specified match.

        :param match: related match object.
        :type match: models.Match.
        :param selections_data: selections data list.
        :param selections_data: list. List of dicts.
        """
        for selection_data in selections_data:
            Selection.objects.create(
                match=match,
                market=self,
                **selection_data
            )


class Match(models.Model):
    """Store match entry.

    Relating to foreign key Sport object,
    and Market object via many-to-many relation."""

    id = models.BigIntegerField(primary_key=True, unique=True)
    url = models.URLField(max_length=200, blank=False)
    name = models.CharField(max_length=200, blank=False)
    starttime = models.DateTimeField(null=False)
    sport = models.ForeignKey(Sport, on_delete=models.CASCADE, null=False)
    markets = models.ManyToManyField(Market, related_name='+')

    @classmethod
    def generate(cls, serializer_data):
        """Generate match instance from serializer data

        :param serializer_data: Posted data passed from serializer.
        :type serializer_data: dict.
        """
        # Applying atomic transaction to make sure that DB entries are kept consistent.
        with transaction.atomic():
            sport = get_object_or_404(Sport, **serializer_data.pop('sport'))
            markets_data = serializer_data.pop('markets')
            match = cls(sport=sport, **serializer_data)
            markets = list(cls._generate_markets(markets_data, match))
            match.markets.set(markets)
            match.save()

    @classmethod
    def _generate_markets(cls, markets_data, match):
        for market_data in markets_data:
            selections_data = market_data.pop('selections')
            market = get_object_or_404(Market, **market_data)
            market.create_selections(match, selections_data)
            yield market

    @staticmethod
    def patch_odds(serializer_data):
        """Update related existing selections odds.

        :param serializer_data: Posted data passed from serializer.
        :type serializer_data: dict.
        """
        # Canonically it must be in PATCH request.
        markets_data = serializer_data.pop('markets')
        for market_data in markets_data:
            selections_data = market_data.pop('selections')
            Match._update_odds(selections_data)

    @staticmethod
    def _update_odds(selections_data):
        for sel_data in selections_data:
            selection = get_object_or_404(Selection, pk=sel_data['id'])
            selection.update_odds(sel_data['odds'])


class Selection(models.Model):
    """Store selection entry, referring Market and Match objects"""

    id = models.BigIntegerField(primary_key=True, unique=True)
    name = models.CharField(max_length=200, blank=False)
    odds = models.FloatField(blank=False)
    market = models.ForeignKey(Market, related_name='selections', on_delete=models.CASCADE, null=False)
    match = models.ForeignKey(Match, on_delete=models.CASCADE, null=False)

    def update_odds(self, new_odds):
        """Update entry odds value.
        :param new_odds: new odds.
        :type new_odds: float.
        """
        self.odds = new_odds
        self.save()
