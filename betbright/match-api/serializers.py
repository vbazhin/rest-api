"""Web API project serializers."""
from .models import Match, Sport, Market, Selection
from rest_framework.exceptions import ValidationError
from rest_framework import serializers
from django.shortcuts import get_object_or_404


class SelectionsSerializer(serializers.HyperlinkedModelSerializer):
    """Selections class serializer."""

    class Meta:
        model = Selection
        fields = ('id', 'name', 'odds')

    @classmethod
    def validate_updating_odds(cls, selections_data, raise_exception=False):
        """Ensure that updating selections data odds posted in a valid format.

        :param selections_data: Selections data dict extracted from the request.
        :param raise_exception: Should exception be raised if data is not valid.
        :type selections_data: dict.
        :type raise_exception: bool.
        :return: Is returned data valid.
        :rtype: bool.
        """
        for sel_data in selections_data:
            odds = sel_data.get('odds', None)
            if 'odds' is None or not cls._is_odd_float_convertable(odds):
                if raise_exception:
                    raise ValidationError('Odds not in selection')
                return False
        return True

    @staticmethod
    def _is_odd_float_convertable(odds):
        try:
            float(odds)
        except ValueError:
            return False
        return True


class MarketSerializer(serializers.HyperlinkedModelSerializer):
    """Market serializer class."""

    selections = serializers.SerializerMethodField('get_related_selections')

    class Meta:
        model = Market
        fields = ('id', 'name', 'selections')

    def get_related_selections(self, market):
        """Modify selection queryset.

        Get selections that refer current marked and requested match.

        :param market: Market object.
        :type market: match-api.models.Market
        :return:
        """
        if self._match_details_retrieved:
            return self._filter_related_selections(market)

    @property
    def _match_details_retrieved(self):
        return self.context['view'].serializer_class == MatchDetailsSerializer

    def _filter_related_selections(self, market):
        parset_context = self.context['request'].parser_context
        match_id = parset_context['kwargs'].get('pk')
        match = get_object_or_404(Match, pk=match_id)
        selections_queryset = Selection.objects.filter(
            match=match, market=market
        )
        serializer = SelectionsSerializer(
            instance=selections_queryset,
            many=True, context=self.context
        )
        return serializer.data

    @staticmethod
    def validate_selections_data(selections_data):
        """Validate each entity of markets selections data in the request.

        Raise ValidationError, if any of the data is invalid.

        :param selections_data: Selections data.
        :type selections_data: dict.
        """
        for sel_data in selections_data:
            SelectionsSerializer(data=sel_data).is_valid(raise_exception=True)

    def is_valid(self, raise_exception=False):
        """Raise error, if market doesn't exist, as creating new markets is not allowed.
        According to the docs, there is only one "hardcoded" market "Winner".

        :param raise_exception: Should exception be raised if data is not valid.
        :type raise_exception: bool.
        """
        markets_data = self.initial_data.get('markets', list())
        for market_data in markets_data:
            exists = Market.objects.filter(id=market_data['id']).exists()
            if not exists:
                if raise_exception:
                    raise ValidationError('Market with id "{}" doesn\'t '
                                          'exist.'.format(market_data['id']))
            return False
        return True


class SportSerializer(serializers.HyperlinkedModelSerializer):
    """Sport model serializer class."""

    class Meta:
        model = Sport
        fields = ('id', 'name')


class MatchListSerializer(serializers.HyperlinkedModelSerializer):
    """Matches list serializer class."""

    startTime = serializers.DateTimeField(source='starttime')

    class Meta:
        model = Match
        fields = ('id', 'url', 'name', 'startTime')


class MatchDetailsSerializer(serializers.HyperlinkedModelSerializer):
    """Match detail serializer.

    Serialises related sport and markets data.
    Markets data contains selections that refer the current match
    and belong to the corresponding market.
    """

    class Events:
        """Store request event params names."""

        NEW_EVENT = 'NewEvent'
        UPDATE_ODDS = 'UpdateOdds'

    markets = MarketSerializer(many=True, read_only=True)
    sport = SportSerializer(read_only=True)
    startTime = serializers.DateTimeField(source='starttime')

    class Meta:
        model = Match
        fields = ('id', 'url', 'name', 'startTime', 'sport', 'markets')

    def is_valid(self, *args, **kwargs):
        """Override parent's validation method to handle validation on "UpdateOdds" message."""
        message_type = self.initial_data.get('message_type')
        if message_type not in (self.Events.NEW_EVENT, self.Events.UPDATE_ODDS):
            raise ValidationError('Invalid message type for POST '
                                  'request: {}'.format(message_type))
        if message_type == self.Events.NEW_EVENT:
            self._validate_markets_data()
            return super().is_valid(*args, **kwargs)
        else:
            self._validate_updated_odds(**kwargs)

    def _validate_updated_odds(self, **kwargs):
        for market_data in self.initial_data['markets']:
            SelectionsSerializer.validate_updating_odds(market_data['selections'], **kwargs)

    def _validate_markets_data(self):
        for market_data in self.initial_data['markets']:
            cleared_market_data = market_data.copy()
            cleared_market_data.get('selections')
            market_serializer = MarketSerializer(data=cleared_market_data)
            market_serializer.is_valid(raise_exception=True)
            market_serializer.validate_selections_data(
                selections_data=market_data['selections']
            )

    def save(self):
        """Save new match or update odds.

        Type of the message ("NewEvent" or "UpdateOdds")
        is kept in "message_type" param,
        """
        request_data = self.initial_data.copy()
        self._fix_starttime_format(request_data)
        message_type = request_data.pop('message_type')
        if message_type == self.Events.NEW_EVENT:
            Match.generate(request_data)
        elif message_type == self.Events.UPDATE_ODDS:
            Match.patch_odds(request_data)

    @staticmethod
    def _fix_starttime_format(request_data):
        request_data['starttime'] = request_data.pop('startTime')
