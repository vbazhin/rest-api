"""The module contains mockups for match-api models tests.

Must be implemented in a real-life project.
"""
from django.test import TestCase
from ..models import (
    Match, Sport, Market, Selection
)


class TestMatch(TestCase):

    def test_generate(self):
        ...

    def test_generate_invalid_serializer_data(self):
        # Multiple cases of invalid data must be tested.
        ...

    def test_patch_odds(self):
        ...

    def test_patch_odds_invalid_serializer_data(self):
        # Multiple cases of invalid data must be tested.
        ...


class TestMarket(TestCase):

    def test_create_selections(self):
        ...

    def test_create_selections_invalid_data(self):
        # Multiple cases of invalid data must be tested.
        ...


class TestSelection(TestCase):

    def test_update_odds(self):
        ...

    def test_update_odds_incorrect_data_type(self):
        ...
