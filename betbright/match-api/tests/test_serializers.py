"""The module contains mockups for match-api serializers tests.

Must be implemented in a real-life project.
"""
from django.test import TestCase
from ..serializers import (
    SelectionsSerializer, MatchListSerializer,
    MatchDetailsSerializer, MarketSerializer
)


class TestMatchDetailSerializer(TestCase):

    def test_correct_field_values(self):
        ...

    def test_incorrect_field_values(self):
        # Multiple separate tests can be implemented to test each specific "negative" case.
        ...

    def test_is_valid(self):
        ...
        # Multiple "negative" scenarios must be tested.

    def test_save(self):
        ...


class TestMarketSerializer(TestCase):

    def test_correct_field_values(self):
        ...

    def test_incorrect_field_values(self):
        ...

    def test_validate_selection_data(self):
        ...

    def test_validate_selection_data_invalid_data(self):
        # It may worth creating multiple separate negative tests for invalid data cases.
        ...

    def test_get_related_selections(self):
        ...


class TestSelectionsSerializer(TestCase):

    def test_correct_field_values(self):
        ...

    def test_incorrect_field_values(self):
        ...

    def test_validate_new_odds(self):
        ...

    def test_validate_new_odds_invalid_type(self):
        ...


class TestMatchListSerializer(TestCase):

    def test_correct_field_values(self):
        ...

    def test_incorrect_field_values(self):
        ...


class TestSportSerializer(TestCase):

    def test_correct_field_values(self):
        ...

    def test_incorrect_field_values(self):
        ...
