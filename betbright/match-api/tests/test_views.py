"""The module implement unit tests for match-api views.

Some of the tests left as mockups that must be implemented for a real project.
"""
import json
import datetime
import unittest
from django.urls import reverse
from rest_framework.test import APITestCase
from ..models import Match, Sport, Market, Selection


class MatchListCreateAPIViewTestCase(APITestCase):

    url = reverse("match-api:list")

    def setUp(self):
        self.default_market = Market.objects.create(id=1, name='Winner')

    def test_new_match(self):
        Sport.objects.create(id=1, name='Football')
        match_data = {
           "message_type": "NewEvent",
            "id": 1,
            "url": "http://12345.com",
            "name": 1,
            "startTime": "1111-02-11 11:11:00",
            "sport": {
                "id": 1,
                "name": "Football"
            },
            "markets": [
                {
                    "id": 1,
                    "name": "Winner",
                    "selections": [
                        {
                            "id": 1,
                            "name": "New",
                            "odds": 1.0
                        }
                    ]
                }
            ]
        }
        response = self.client.post(self.url, match_data, format='json')
        self.assertEqual(200, response.status_code)

    def test_list_matches(self):
        default_sport = Sport.objects.create(id=1, name='Football')
        for i in range(10):
            Match.objects.create(
                id=i,
                name='New Match',
                starttime=datetime.datetime(1111, 11, 11, 11, 11),
                sport=default_sport,
                url='http://test.com'
            )
        response = self.client.get(self.url, format='json')
        self.assertTrue(len(json.loads(response.content)) == Match.objects.count())

    def test_list_matches_filters(self):
        default_sport = Sport.objects.create(id=1, name='Football')
        Match.objects.create(
            id=1,
            name='Match 1',
            starttime=datetime.datetime(1111, 11, 11, 11, 11),
            sport=default_sport,
            url='http://test.com'
        )
        Match.objects.create(
            id=2,
            name='Match 2',
            starttime=datetime.datetime(1111, 11, 11, 11, 11),
            sport=default_sport,
            url='http://test.com'
        )
        new_sport = Sport.objects.create(id=2, name='Baseball')
        Match.objects.create(
            id=3,
            name='Match 3',
            starttime=datetime.datetime(1111, 11, 11, 11, 11),
            sport=new_sport,
            url='http://test.com'
        )
        sport_filter_response = self.client.get(self.url + '?sport=Football')
        self.assertTrue(
            len(json.loads(sport_filter_response.content)) ==
            Match.objects.filter(sport__name='Football').count()
        )
        name_filter_response = self.client.get(self.url + '?name=Match%201')
        self.assertTrue(
            len(json.loads(name_filter_response.content)) ==
            Match.objects.filter(name='Match 1').count()
        )
        multiple_filter_response = self.client.get(self.url + '?name=Match 1&sport=Baseball')
        self.assertTrue(
            len(json.loads(multiple_filter_response.content)) ==
            Match.objects.filter(name='Match 1', sport__name='Baseball').count()
        )

    def test_list_matches_ordering(self):
        """Note ordering works in descending order, as shown at page 2 of the docs."""
        sport1 = Sport.objects.create(id=1, name='Acrobatics')
        sport2 = Sport.objects.create(id=2, name='Baseball')
        sport3 = Sport.objects.create(id=3, name='Curling')
        Match.objects.create(
            id=1,
            name='Match 1',
            starttime=datetime.datetime(1111, 3, 11, 11, 11),
            sport=sport1,
            url='http://test3.com'
        )
        Match.objects.create(
            id=2,
            name='Match 2',
            starttime=datetime.datetime(1111, 1, 11, 11, 11),
            sport=sport3,
            url='http://test1.com'
        )
        Match.objects.create(
            id=3,
            name='Match 03',
            starttime=datetime.datetime(1111, 2, 11, 11, 11, 11),
            sport=sport2,
            url='http://test2.com'
        )
        unordered_response = self.client.get(self.url)
        self.assertEqual([i['id'] for i in unordered_response.data], [1, 2, 3])
        ordered_by_starttime_response = self.client.get(self.url + '?ordering=startTime')
        self.assertEqual([i['id'] for i in ordered_by_starttime_response.data], [1, 3, 2])
        ordered_by_name_response = self.client.get(self.url + '?ordering=name')
        self.assertEqual([i['id'] for i in ordered_by_name_response.data], [2, 1, 3])
        ordered_by_url_response = self.client.get(self.url + '?ordering=url')
        self.assertEqual([i['id'] for i in ordered_by_url_response.data], [1, 3, 2])
        ordered_by_sport_response = self.client.get(self.url + '?ordering=sport')
        self.assertEqual([i['id'] for i in ordered_by_sport_response.data], [2, 3, 1])
        # Testing case insensitibility.


    def test_update_odds(self):
        sport = Sport.objects.create(id=1, name='Football')
        match = Match.objects.create(
            id=1,
            name='New Match',
            starttime=datetime.datetime(1111, 11, 11, 11, 11),
            sport=sport,
            url='http://test.com'
        )
        match.markets.set([self.default_market])
        match.save()
        Selection.objects.create(
            id=1,
            name='Selection1',
            odds=1,
            market=self.default_market,
            match=match
        )
        Selection.objects.create(
            id=2,
            name='Selection2',
            odds=2,
            market=self.default_market,
            match=match
        )
        new_valid_odds1 = 5.0
        new_valid_odds2 = 10.0
        update_odds_data = {
            "message_type": "UpdateOdds",
            "id": 1,
            "url": "http://test.com",
            "name": "New Match",
            "startTime": "1111-11-11 11:11:00 ",
            "sport": {
                "id": 1,
                "name": "Football"
            },
            "markets": [
                {
                    "id": 1,
                    "name": "Winner",
                    "selections": [
                        {
                            "id": 1,
                            "name": "Selection1",
                            "odds": new_valid_odds1
                        },
                        {
                            "id": 2,
                            "name": "Selection2",
                            "odds": new_valid_odds2
                        }
                    ]
                }
            ]
        }

        response = self.client.post(self.url, data=update_odds_data, format='json')
        self.assertEqual(Selection.objects.get(id=1).odds, new_valid_odds1)
        self.assertEqual(Selection.objects.get(id=2).odds, new_valid_odds2)
        self.assertEqual(200, response.status_code)

        # Making sure that a corresponding error raised and selections value not updated,
        # if selection format is invalid
        update_odds_data['markets'][0]['selections'][0]['odds'] = 'invalid_odds_value'
        response = self.client.post(self.url, data=update_odds_data, format='json')
        self.assertEqual(400, response.status_code)
        self.assertEqual(Selection.objects.get(id=1).odds, new_valid_odds1)

    # TODO: Tests below must be implemented for real-life implementation.
    # The list of tests mocks below must test the negative scenarios,
    # when invalid data passed in the request. That's most bulky part of the unit tests,
    # especially the tests for match creation and update.
    # For purposes of simplification of the data population I would recommend to use
    # built-in module "mock" (or unittests.mock), third modules like
    # "factory" (factory.django), or/and use django fixtures.

    def test_list_matches_invalid_id(self):
        ...

    def test_list_matches_invalid_filter(self):
        ...

    def test_list_matches_invalid_ordering(self):
        ...

    def test_create_match_invalid_data(self):
        ...


class MatchDetailAPIViewTestCase(APITestCase):

    def setUp(self):
        self.default_sport = Sport.objects.create(id=1, name='Football')
        self.match = Match.objects.create(
            id=1,
            name='New Match',
            starttime=datetime.datetime(1111, 11, 11, 11, 11),
            sport=self.default_sport,
            url='http://test.com'
        )
        self.default_market = Market.objects.create(id=1, name='Winner')
        self.match.markets.set([self.default_market])
        self.match.save()
        Selection.objects.create(
            id=1,
            name='Selection1', odds=1,
            market=self.default_market,
            match=self.match
        )
        Selection.objects.create(
            id=2,
            name='Selection2', odds=2,
            market=self.default_market,
            match=self.match
        )
        self.url = reverse("match-api:detail", kwargs={"pk": 1})

    def test_get_match_details(self):
        expected_data = {
            "id": 1,
            "url": "http://test.com",
            "name": "New Match",
            "startTime": "1111-11-11 11:11:00",
            "sport": {
                 "id": 1,
                 "name": "Football"
            },
            "markets": [
                {
                    "id": 1,
                    "name": "Winner",
                    "selections": [
                        {
                            "id": 1,
                            "name": "Selection1",
                            "odds": 1.0
                        },
                        {
                            "id": 2,
                            "name": "Selection2",
                            "odds": 2.0
                        }
                    ]
                }
            ]
        }
        response = self.client.get(self.url)
        self.assertEqual(json.loads(response.content), expected_data)
        self.assertEqual(200, response.status_code)

    def test_get_match_details_invalid_id(self):
        bad_url = reverse("match-api:detail", kwargs={"pk": 2})
        response = self.client.get(bad_url)
        self.assertEqual(404, response.status_code)


def suite():
    suite = unittest.TestSuite()
    suite.addTests(unittest.makeSuite(
        MatchListCreateAPIViewTestCase,
        MatchDetailAPIViewTestCase
    ))


if __name__ == '__main__':
    unittest.main()
