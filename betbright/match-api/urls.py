"""Web REST API urls module."""

from django.conf.urls import url
from .views import MatchListCreateView, MatchDetailsView


app_name = 'match-api'

urlpatterns = [
    url(r'^match/$', MatchListCreateView.as_view(), name='list'),
    url(r'^match/(?P<pk>\d+)/$', MatchDetailsView.as_view(), name='detail'),
]
