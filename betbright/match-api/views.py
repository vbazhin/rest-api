"""
Web API Views module.

The views implemented on the basis of django-rest-framework generic views.
"""
import copy
from rest_framework.response import Response
from rest_framework import generics
from .models import Match
from .serializers import (
    MatchDetailsSerializer,
    MatchListSerializer
)
from .filters import MatchOrderingFilter


class MatchDetailsView(generics.RetrieveAPIView):
    """Match object details view."""

    queryset = Match.objects.all()
    serializer_class = MatchDetailsSerializer


class MatchListCreateView(generics.ListCreateAPIView):
    """Match object list view.

    The view handles Matches list request on GET request,
    as well as Match creation and odds update on POST request.
    """

    serializer_class = MatchListSerializer
    filter_backends = (MatchOrderingFilter, )

    def post(self, request, *args, **kwargs):
        """Post request handler.

        According to the coding challenge description, "UpdateOdds" sent in the request data,
        so I suppose, it was implied to be sent in POST request as well as "NewEvent".
        However, canonically data update is send on PATCH request in the object namespace.

        :param request: request object.
        :type request: rest_framework.request.Request
        :return: post response
        :rtype: rest_framework.response.Response
        """
        serializer = MatchDetailsSerializer(data=copy.deepcopy(request.data))
        serializer.is_valid(raise_exception=True)
        serializer.save()
        headers = self.get_success_headers(copy)
        return Response(request.data, headers=headers)

    def get_queryset(self):
        """Filter/order matches queryset.

        Queryset ordering is handled by rest_framework.filters.OrderingFilter.
        Filter can be mapped using 3rd party module "django-url-filters",
        as shown: https://stackoverflow.com/a/36523017,
        but for this particular case (according to the coding challenge docs,
        filtering should be supported for "name" and "sport" fields only),
        it's easier to do it manually.
        """
        queryset = Match.objects.all()
        sport_name = self.request.query_params.get('sport', None)
        if sport_name is not None:
            # Case insensitive search.
            queryset = queryset.filter(sport__name__iexact=sport_name)
        match_name = self.request.query_params.get('name', None)
        if match_name is not None:
            queryset = queryset.filter(name__iexact=match_name)
        ordering = self.request.query_params.get('ordering', None)
        # Performing ordering by sport name.
        # For rest of the fields the details OrderingFilter works from the box, bit
        if ordering == '-sport':
            queryset = queryset.order_by('{}__name'.format(ordering.lstrip('-')))
        elif ordering == 'sport':
            queryset = queryset.order_by('-{}__name'.format(ordering))
        return queryset
